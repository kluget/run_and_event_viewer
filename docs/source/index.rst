.. ReView documentation master file, created by
   sphinx-quickstart on Sat Feb 19 16:54:47 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the ReView documentation!
================================================

The Run and Event Viewer (aka Review) enables scientists to retrieve camera data structured in groups, called *runs*.  

Each trigger that leads to data collection within a run is called *event*. ReView assumes the following types of events: *preshots*, *mainshots*, *postshots*, *darkfield*. Each event can contain several camera images identified by a unique string ``[camera]``.  

A run is defined either via the file numbering scheme: ``[filePrefix][run_number][separator][event_number][fileEnd][camera].[fileType]`` or via the definition in a labbook aka :ref:`google spread sheet <google spreadsheet setup>`. 

Currently supported file types are tif, npz, npy, txt. Currently supported data sources are local, SSH, API (extra_mmm wrapper for European XFEL). 

Table of contents
=================

.. toctree::
   
   usage
   settings
   spreadsheet
   plotting
   classes


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
