.. _Plotting:

Plotting
========

Plotting is done using the processed images by default. However, also the raw images can be plotted. 
The main idea is that central aspects are set through the google labbook, accessible for all. Basically that serves as central vizualization metadata catalogue. 
The other idea is that you don't have to care wether you are dealing with an :class:`review.Event`, :class:`review.Run`, or :class:`review.Experiment` object. 
All three have the same methods plot_overview --> plot_detector and plot_profile and plot all respective events in the hirarchy. 

Before reading on, you might want get familiar with the image processing method :meth:`review.Event.get_processed_image`. 
There are basically three ways to use predefined plotting routines: imageJ, matplotlib, and QT, described below. 
The hirarchy of plotting using matplotlib is plot_overview --> plot_detector and plot_profile --> (get_profile ->) (get_processed_image ->) get_image.
The hirarchy for open_in_imageJ is You can also plot your own stuff using the methods :meth:`review.Event.get_processed_image` and :meth:`review.Event.get_profile`. 

imageJ
------

It is easy to open all Experiment, a substet of Experiment, a single run, or an single event in imageJ. You can even open only the mainshots or another shot type of Experiment. 

To open events in in imageJ, just call the ``open_in_imageJ()`` method of the ``Experiment()``, ``Run()`` or ``Event()`` object. Review will try to group Experiment in imageJ-stacks. Details can be found in :meth:`Experiment.open_in_imageJ`, :meth:`Run.open_in_imageJ`, :meth:`Event.open_in_imageJ`

.. note::
	The first time the method is called, imageJ is downloaded and initilaized in the background. This can take a few seconds to a minute.
	
Example::

	Open all Experiment in imageJ:
    
	.. code-block:: python
    
		import review as rv
		experiment = rv.Experiment()
		experiment.update()
		
		experiment.open_in_imageJ()

	Open only the mainshots of the Experiment:
    
	.. code-block:: python
    
		import review as rv
		experiment = rv.Experiment()
		experiment.update()
		
		experiment.open_in_imageJ(shot_type="mainshot")

	Open only a certain run:
    
	.. code-block:: python
    
		import review as rv
		experiment = rv.Experiment()
		experiment.update()
		
		experiment[123].open_in_imageJ(shot_type="mainshot")

	Open only a certain event:
    
	.. code-block:: python
    
		import review as rv
		experiment = rv.Experiment()
		experiment.update()
		
		experiment[123][124].open_in_imageJ(shot_type="mainshot")

inline
------

.. note::
	There is a very recent bug in matplotlib described `here <https://github.com/matplotlib/matplotlib/issues/22576>`: 'AnchoredSizeLocator' object has no attribute 'get_subplotspec'. If you get this, open review.py and search for 'colorbar'. Comment all related lines.



GUI
---