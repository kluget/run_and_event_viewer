import numpy as np
import matplotlib.pyplot as plt
import functools
from scipy import ndimage
from IPython import embed


#plt.rcParams['savefig.facecolor'] = "0.8"

class ProfilePicker:
    def __init__(self, im, ax1, ax2, ax3, data, width=10):
        self.im = im
        self.data = data
        self.width = width
        self.xpos = {}
        self.ypos = {}
        self.x_profile = {}
        self.y_profile = {}
        click_function = functools.partial(self.on_mouse_click, ax1=ax1, ax2=ax2, ax3=ax3)
        self.cid_button = im.figure.canvas.mpl_connect('button_press_event', click_function)
        #self.cid_key = im.figure.canvas.mpl_connect('key_press_event', self.on_key_press)
        
    def on_mouse_click(self, event, ax1, ax2, ax3):
        """Draws x- and y-direction profile starting from mouse position.
           Input:   ax    ... plot where we can click and data is displayed
                    ax2   ... plot for y_profile
                    ax3   ... plot for x_profile
                    data  ... processed (rotated) image
                    width ... witdth of profile, over which the mean is calculated
        """

        # Check, if clicked with left mouse button and in axis, aswell as if zoom 
        # function is deactivated:
        #mode = event.canvas.toolbar.mode
        if event.button == 1 and event.inaxes == ax1:#and mode == '':# 

            self.xpos = round(event.xdata)   # gets colum / row of image where we click
            self.ypos = round(event.ydata)

            # hard coded x axis REPLACE
            x = np.linspace(0, self.data.shape[0], self.data.shape[0])

            # get profiles
            self.x_profile = self.data[int(self.ypos-self.width/2) : int(self.ypos+self.width/2), ::]
            self.x_profile = np.mean(self.x_profile, axis=0)
            self.y_profile = self.data[::, int(self.xpos-self.width/2) : int(self.xpos+self.width/2)]
            self.y_profile = np.mean(self.y_profile, axis=1)

            # norm (+ 1e-10 to avoid /0)
            self.x_profile /= np.max(self.x_profile + 1e-10)
            self.y_profile /= np.max(self.y_profile + 1e-10)

            # empty profile-plots
            ax1.lines = []
            ax2.lines = []
            ax3.lines = []
            # draw lines along axis
            ax1.plot([self.xpos, self.xpos], [0, self.data.shape[1]], c='r', lw=1, ls = '--')
            ax1.plot(x, self.ypos*np.ones(self.data.shape[1]), c='b', lw=1, ls = '--')
            ax2.plot(self.y_profile, x, lw=1, c='r', marker='o', ms=0)      # add graphs
            ax3.plot(self.x_profile, lw=1, c='b', marker='o', ms=0) 
            event.canvas.draw()  

class LineBuilder:
    def __init__(self, line, ax):
        self.line = line
        self.first_line = []
        self.second_line = []
        self.xs = list(line.get_xdata())
        self.ys = list(line.get_ydata())
        key_function = functools.partial(self.on_key_press, ax=ax)
        self.cid_button = line.figure.canvas.mpl_connect('button_press_event', self.on_button_press)
        self.cid_key = line.figure.canvas.mpl_connect('key_press_event', key_function)

    def on_button_press(self, event):
        #mode = event.canvas.toolbar.mode
        if event.button == 1 and event.inaxes == self.line.axes:# and mode == '':
            if len(self.xs) >= 2:
                self.xs = []
            if len(self.ys) >= 2:
                self.ys = []
            self.xs.append(event.xdata)
            self.ys.append(event.ydata)
            self.line.set_data(self.xs, self.ys)
            self.line.figure.canvas.draw()
            #print(f'line = {self.line.get_data()}')
            
    def on_key_press(self, event, ax):
        #mode = event.canvas.toolbar.mode
        if event.key == 'c':# and mode == '':
            if len(self.first_line) == 0:
                self.first_line.append(self.line.get_data())
                #print(f'first line 1 = {self.first_line}')
                ax.plot(self.first_line[0][0], self.first_line[0][1])
                print('Please select two lines')
            elif len(self.first_line) != 0 and len(self.second_line) == 0:
                self.second_line.append(self.line.get_data())
                
                ax.plot(self.second_line[0][0], self.second_line[0][1])
                #print(f'first line 2 = {self.first_line}')
                #print(f'second line 2 = {self.second_line}')
                print('Two lines have been selected. To delete this choice press c again')
                print()
            else:
                self.first_line = []
                self.second_line = []
                if len(ax.lines) > 2:
                   del ax.lines[1:] 
 
            self.line.figure.canvas.draw()
   
        if event.key != 'c':
            print('Please press c to confirm a line')           

class SmoothProfile:
    def __init__(self, x_profile, y_profile, bins=10):
        
        self.bins = bins
    
        extend_x = bins - len(x_profile)%bins
        extend_y = bins - len(y_profile)%bins
        
        x_profile = np.concatenate((x_profile, np.zeros(extend_x) + x_profile[-1]))
        y_profile = np.concatenate((y_profile, np.zeros(extend_y) + y_profile[-1]))
        x_profile_reshape =  x_profile.reshape((-1, bins))
        y_profile_reshape =  y_profile.reshape((-1, bins))
        
        self.x_profile_smooth = np.mean(x_profile_reshape, axis=1)
        self.y_profile_smooth = np.mean(y_profile_reshape, axis=1)
    
        
def main():
    """main program"""
    
    angle = input("Please select the angle of rotiation (clockwise): angle = ")
    angle = float(angle)
    width = input("Please select the width of the profile: width = ")
    width = float(width)
    #angle = 0
    #width = 10
    # Hard coded data, REPLACE
    data = np.random.rand(2048, 2048)
    for i in range(1020, 1030):
        data[i, ::] = np.linspace(0, 1, 2048)
        data[::, i] = np.linspace(0, 1, 2048)**2
    
    #embed() ################################################################
    
    # rotating data
    data_rotate = ndimage.rotate(data, angle)   
     
    # format graphs
    fig = plt.figure(figsize=(6.4, 5.8))
    
    xInt = [0, data_rotate.shape[0]]
    yInt = [0, data_rotate.shape[1]]
    Int  = xInt + yInt
    
    ax1 = plt.subplot2grid((3, 3), (0, 0), colspan=2, rowspan=2)
    ax1.axis(Int)
    #plt.imshow(data_rotate, origin='lower', aspect='auto')
    ax2 = plt.subplot2grid((3, 3), (0, 2), rowspan=2, sharey = ax1)
    ax3 = plt.subplot2grid((3, 3), (2, 0), colspan=2, sharex = ax1)

    ax1.set_title('data', fontsize=12)
    ax2.set_title('y-profile', fontsize=12)
    ax3.set_title('x-profile', fontsize=12)

    # connecting mouse_click function
    im = ax1.imshow(data_rotate, origin='lower', aspect='auto')
    profilepicker = ProfilePicker(im=im, ax1=ax1, ax2=ax2, ax3=ax3, data=data_rotate)

    plt.tight_layout()
    plt.show()
    
    xpos = profilepicker.xpos
    ypos = profilepicker.ypos
    x_profile = profilepicker.x_profile 
    y_profile = profilepicker.y_profile 
    
     # Output of program doc-string and of the parameter
    print(__doc__)                                           
    print("Parameter:")                                      
    print("Angle of rotation: angle = {}".format(angle))
    print("width of profiles: width = {}".format(width))  
    print("x position: xpos = {}".format(xpos)) 
    print("y position: ypos = {}".format(ypos))  
    
    
    # safe profiles
    
    #x_profile = data_rotate[int(ypos-width/2) : int(ypos+width/2), ::]
    #x_profile = np.mean(x_profile, axis=0)
    #y_profile = data_rotate[::, int(xpos-width/2) : int(xpos+width/2)]
    #y_profile = np.mean(y_profile, axis=1)
    #x_profile /= np.max(x_profile + 1e-10)
    #y_profile /= np.max(y_profile + 1e-10)
   
   

    #print(profiles)
    fig2 = plt.figure(figsize=(6.4, 5.8))
    ax = fig2.add_subplot(211)
    ax2 = fig2.add_subplot(212)
    ax.set_title('x-profile', fontsize=12)
    ax2.set_title('y-profile', fontsize=12)
    ax.plot(x_profile, lw=1, c='b', marker='o', ms=0)
    ax2.plot(y_profile, lw=1, c='r', marker='o', ms=0) 
    
    # smooth profiles
    bins = 10
    smooth_profile = SmoothProfile(x_profile=x_profile, y_profile=y_profile, bins=bins)
    x_profile_smooth = smooth_profile.x_profile_smooth
    y_profile_smooth = smooth_profile.y_profile_smooth
    
    fig3 = plt.figure(figsize=(6.4, 5.8))
    ax3 = fig3.add_subplot(211)
    ax4 = fig3.add_subplot(212)
    ax3.set_title(f'smoothed x-profile, bins size = {bins}', fontsize=12)
    ax4.set_title(f'smoothed y-profile, bin size = {bins}', fontsize=12)
    ax3.plot(np.linspace(0, len(x_profile_smooth), len(x_profile_smooth))*bins, 
             x_profile_smooth, lw=1, c='b', marker='o', ms=0)
    ax4.plot(np.linspace(0, len(y_profile_smooth), len(y_profile_smooth))*bins, 
             y_profile_smooth, lw=1, c='r', marker='o', ms=0)
    
    plt.tight_layout()
    plt.show() 
    
    ############################# CENTER TRANSFORMATION ######################

    old_center = [1023, 2037]

    data_rotate = ndimage.rotate(data.T, angle)   



    old_vector = [old_center[0]-data.shape[0]/2, old_center[1]-data.shape[1]/2]
    old_x, old_y = old_vector

    # meassure angle from virtual center straight up (=0), clockwise
    
    if old_vector == [0, 0]:
        new_vector = [0, 0]
    elif abs(old_x) + abs(old_y) >= 2*abs(old_x) and old_x >= 0:
        angle_old = np.arccos(old_y / np.sqrt(old_x**2 + old_y**2)) / np.pi*180
    elif abs(old_x) + abs(old_y) >= 2*abs(old_x) and old_x < 0:
        angle_old = - np.arccos(old_y / np.sqrt(old_x**2 + old_y**2))/np.pi*180+360
    elif abs(old_x) + abs(old_y) < 2*abs(old_x) and old_x >= 0:
        angle_old = np.arccos(old_y / np.sqrt(old_x**2 + old_y**2)) / np.pi*180
    elif abs(old_x) + abs(old_y) < 2*abs(old_x) and old_x < 0:
        angle_old = - np.arccos(old_y / np.sqrt(old_x**2 + old_y**2))/np.pi*180+360

    if old_vector != [0, 0]:
        angle_new = (angle_old + angle)%360
        length_vector = np.sqrt(old_x**2 + old_y**2) # = virtual to old/new center
        new_x = length_vector * np.sin(angle_new / 180 *np.pi)
        new_y = length_vector * np.cos(angle_new / 180 *np.pi)
        new_vector = [new_x, new_y]

    new_center = [new_x + data_rotate.shape[0]/2, new_y + data_rotate.shape[1]/2]   
    print(f'vector_old = {old_vector}')
    print(f'data.shape = {data.shape}')
    print(f'data_rotate.shape = {data_rotate.shape}')
    if old_vector != [0, 0]:
        print(f'angle_old = {angle_old}')
        print(f'angle_new = {angle_new}')
        print(f'new_vector = {new_vector}')
    print(f'new_center = {new_center}')


    fig4 = plt.figure(figsize=(6.4, 10))
    ax = fig4.add_subplot(211)
    ax2 = fig4.add_subplot(212)
    im = ax.imshow(data.T, origin='lower', aspect='auto')
    im2 = ax2.imshow(data_rotate, origin='lower', aspect='auto')
    plt.tight_layout()
    plt.show()
    
    ############################### STRECHING #################################

            
    fig, ax = plt.subplots()
    xInt = [-10, 10]
    yInt = [-10, 10]
    Int  = xInt + yInt
    ax.axis(Int)
    ax.set_title('Click to build line segments and confirm choice by pressing "c"')
    line, = ax.plot([])  # empty line
    linebuilder = LineBuilder(line=line, ax=ax)

    plt.show()
    # output in Form von [([x1, x2], [y1, y2])]
    print(f'first line = {linebuilder.first_line}')
    print(f'second line = {linebuilder.second_line}')
    
if __name__ == "__main__":
    main()



