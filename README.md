# Run And Event Viewer

## Installation
Currently, you have to download the repository and solve the dependencies manually:

1. fork
2. Clone the repo to your local machine (or to [maxwell](https://max-jhub.desy.de/user/kluget/lab/tree/GPFS/exfel/exp/HED/202201/p002818/usr/Software/overview)), using git:

    ``clone https://gitlab.hzdr.de/kluget/run_and_event_viewer.git

3. (optional) Switch to the ``dev`` or ``nightly`` branch, to get the latest nightly fixes

4. Create a new branch, e.g. ``[dev-your name]``

    ``git checkout -b ``[name_of_your_new_branch]``
    ``git push origin [name_of_your_new_branch]``

5. start using the software as explained in the documentation
6. If there are updates, simply 
    
    - rebase your fork:
    
        **Step 1: Add the remote (original repo that you forked) and call it “upstream”**

        ``git remote add upstream https://gitlab.hzdr.de/kluget/run_and_event_viewer.git``

        **Step 2: Fetch all branches of remote upstream**

        ``git fetch upstream``

        **Step 3: Rewrite your main with upstream’s main/dev/nightly using git rebase.**

        ``git rebase upstream/[main/dev/nightly]`` 

    - pull the changes to your local machine:

        ``git pull``

## Documentation
The documentation can be found [here](http://www.t-kluge.de/review).

## Contribute!
To contribute, please fork this project, then clone to your computer.  
For each new feature, create a new branch first! To merge your changes into the mainline hosted at https://gitlab.hzdr.de/kluget/run_and_event_viewer/-/tree/main, first create a pull request to the dev branch.
